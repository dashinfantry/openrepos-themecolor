#!/bin/sh

BASENAMES=openrepos-themecolor
LANGS="en_IE en_GB de"

#RELEASE_OPT = -nounfinished -silent -compress -idbased -markuntranslated !
# do not use -compress, this does not work!
#RELEASE_OPT = -idbased
RELEASE_OPT="-nounfinished -silent -removeidentical"

for f in "${BASENAMES}"; do
  for l in ${LANGS}; do
    lupdate -recursive qml -target-language "$l" -ts translations/"${f}"-"${l}".ts && lrelease ${RELEASE_OPT} translations/"${f}"-"${l}".ts
  done
done
