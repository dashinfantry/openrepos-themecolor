import QtQuick 2.6
import Sailfish.Silica 1.0
Label {
  anchors.horizontalCenter: parent.horizontalCenter
  width: parent.width
  color: Theme.secondaryColor
  font.pixelSize: Theme.fontSizeSmall
  horizontalAlignment: Text.AlignJustify
  wrapMode: Text.WordWrap
}

// vim: expandtab ts=4 st=4
