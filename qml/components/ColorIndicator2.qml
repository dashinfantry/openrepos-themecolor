import QtQuick 2.6
import Sailfish.Silica 1.0

Icon {
  property color iconcolor
  sourceSize.height: Theme.iconSizeMedium;
  source: "image://theme/icon-m-ambience?" + iconcolor
}

// vim: expandtab ts=4 st=4
