import QtQuick 2.6
import Sailfish.Silica 1.0
import "../components"

Page {
  id: helppage

  allowedOrientations: (devicemodel === 'planetgemini') ?  Orientation.LandscapeInverted : defaultAllowedOrientations

  SilicaFlickable {
    contentHeight: col.height
    anchors.fill: parent
    VerticalScrollDecorator {}
    Column {
        id: col
        width: parent.width - Theme.horizontalPageMargin * 2
        anchors.horizontalCenter: parent.horizontalCenter
        spacing: Theme.paddingLarge
        PageHeader { id: head ; title: qsTr("How to Use") }
        Separator {
          anchors.horizontalCenter: parent.horizontalCenter
          horizontalAlignment : Qt.AlignHCenter
          width: parent.width
          color: Theme.rgba(Theme.highlightBackgroundColor, Theme.opacityFaint)
        }
        SectionHeader {text: qsTr("General")   }
        HelpLabel {
          text: qsTr("This application allows you to modify the current color scheme of Lipstick. It will not (yet) however, change or create new ambiences, \
          nor will your changes survive an Ambience change, Lipstick restart, or device reboot.<br />
          Currently, only some colors can be edited.\
          There are other colors in use by the system, which are autocomputed from the basic four, and can not be modified at the moment.<br/><br/>
          We are working on overcoming some of these limitations.")
        }
        SectionHeader {text: qsTr("The Showroom")   }
        HelpLabel {
          text: qsTr("The Top area on the first page (\"Showroom\") is non-interactive\
          and just shows the colors that are selected currently.<br />\
          Here you can preview your creation.\
          ") 
        }
        SectionHeader {text: qsTr("The Laboratory") }
        HelpLabel {
          text: qsTr("In Slider Input Mode, use the Sliders in the lower section to adjust the colors.<br />\
            In Text Input Mode, you can enter color values directly.<br />
            Check what your theme will look like in the Showroom display.<br />\
            <br />\
            When you're done, use the PullUp Menu or the PushUp Menu to apply your creation to the Session.
          ")
        }
        SectionHeader {text: qsTr("The Cupboard") }
        HelpLabel {
          text: qsTr("This area allows you to store your created palettes for re-use later. \
            There is one global Cupboard, and one specific for the current ambience.<br /> \
            Note that only system-wide Ambiences have a name, custon ones will show as anonymous (for now)")
        }
    }
  }
}

// vim: expandtab ts=4 st=4
