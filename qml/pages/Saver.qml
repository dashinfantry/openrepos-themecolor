import QtQuick 2.6
import Sailfish.Silica 1.0
import Nemo.Configuration 1.0
import "../components"

Page {
  id: saver

  property alias slots: store.slots
  property alias lootboxtapped: store.lootboxtapped

  ConfigurationGroup {
      id: store
      path: "/org/nephros/openrepos-themecolor/storage"
      //synchronous: true
      property int slots: 4
      property int lootboxtapped: 0
  }

  function loadTheme(i) {
      MyPalette.primaryColor             = store.value("store" + i + "_p")
      MyPalette.secondaryColor           = store.value("store" + i + "_s")
      MyPalette.highlightColor           = store.value("store" + i + "_h")
      MyPalette.secondaryHighlightColor  = store.value("store" + i + "_sh")
      MyPalette.highlightBackgroundColor = store.value("store" + i + "_hbg")
      pageStack.navigateBack()
  }
  function saveTheme(i) {
      store.setValue("store" + i + "_p",    MyPalette.primaryColor);
      store.setValue("store" + i + "_s",    MyPalette.secondaryColor);
      store.setValue("store" + i + "_h",    MyPalette.highlightColor);
      store.setValue("store" + i + "_sh",   MyPalette.secondaryHighlightColor);
      store.setValue("store" + i + "_hbg",  MyPalette.highlightBackgroundColor);
      //store.sync();
      //pageStack.navigateBack()
  }
  function setThemeProp(i, prop, val) {
      store.setValue("store" + i + "_" + prop, val);
  }
  function getThemeProp(i, prop) {
      return store.value("store" + i + "_" + prop);
  }

  Component.onCompleted: {
    if ( lootboxtapped >= 3 ) {
      slots += 2;
      lootboxtapped = 0;
    }
  }

  onStatusChanged: {
    // SaverPlus depends on being Attached! so it can pop()
    if ( status === PageStatus.Active ) { pageStack.pushAttached( Qt.resolvedUrl("SaverPlus.qml")) }
  }

  allowedOrientations: (devicemodel === 'planetgemini') ?  Orientation.LandscapeInverted : defaultAllowedOrientations

  SilicaFlickable {
    id: flick
    anchors.fill: parent
    contentHeight: parent.height
    VerticalScrollDecorator {}
    PageHeader { id: head ; title: qsTr("Global Cupboard") }
    Separator {
      id: sep
       anchors.horizontalCenter: parent.horizontalCenter
       anchors.top: head.bottom
       width: parent.width
       //height: 2
       color: Theme.secondaryColor
    }
    SilicaListView {
      id: view
      anchors.top: head.bottom
      anchors.horizontalCenter: parent.horizontalCenter
      width: parent.width
      height: flick.height - (head.height + sep.height )
      clip: true
      currentIndex: -1
      model: slots
      delegate: SaveSlot {
        idx: index
        width: ListView.view.width - Theme.itemSizeSmall * 2
      }
      pullDownMenu: pull
      //footerPositioning: ListView.OverlayFooter
      footer: LootBoxItem {
        id: loot
        numtapped: lootboxtapped
        onNumtappedChanged: { lootboxtapped = numtapped; }
      }
    }
    RemorsePopup { id: remorse }
    PullDownMenu {
      id: pull
      MenuItem { text: qsTr("Clean out this cupboard"); onClicked: remorse.execute(qsTr("Spring Clean..."), 
        function() {
          var tmp = slots;
          store.clear();
          store.setValue("slots", tmp);
          pageStack.navigateBack();
        } ,6000) }
    }
  }
}

// vim: expandtab ts=4 st=4
