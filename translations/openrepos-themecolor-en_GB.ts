<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_GB">
<context>
    <name>ColorField</name>
    <message>
        <location filename="../qml/components/ColorField.qml" line="10"/>
        <source>specify RGB or aRGB value, e.g.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/ColorField.qml" line="10"/>
        <source>(the # is optional)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ColorRandomizer</name>
    <message>
        <location filename="../qml/components/ColorRandomizer.qml" line="20"/>
        <source>Random</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/ColorRandomizer.qml" line="20"/>
        <location filename="../qml/components/ColorRandomizer.qml" line="35"/>
        <location filename="../qml/components/ColorRandomizer.qml" line="56"/>
        <source>Colors</source>
        <translation>Colours</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorRandomizer.qml" line="30"/>
        <location filename="../qml/components/ColorRandomizer.qml" line="51"/>
        <location filename="../qml/components/ColorRandomizer.qml" line="72"/>
        <source>Generated</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/ColorRandomizer.qml" line="35"/>
        <source>Bright</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/ColorRandomizer.qml" line="35"/>
        <source>Dark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/ColorRandomizer.qml" line="56"/>
        <source>Gray</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ColorSelectors</name>
    <message>
        <source>Color</source>
        <translation type="vanished">Colour</translation>
    </message>
    <message>
        <source>Primary Color</source>
        <translation type="vanished">Primary Colour</translation>
    </message>
    <message>
        <source>Secondary Color</source>
        <translation type="vanished">Secondary Colour</translation>
    </message>
    <message>
        <source>Highlight Color</source>
        <translation type="vanished">Highlight Colour</translation>
    </message>
    <message>
        <source>Secondary Highlight Color</source>
        <translation type="vanished">Secondary Highlight Colour</translation>
    </message>
</context>
<context>
    <name>ColorSlider</name>
    <message>
        <location filename="../qml/components/ColorSlider.qml" line="42"/>
        <source>Adjust sliders, tap to reset</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ColorSliders</name>
    <message>
        <source>Color</source>
        <translation type="vanished">Colour</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSliders.qml" line="13"/>
        <source>Primary Color</source>
        <translation>Primary Colour</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSliders.qml" line="18"/>
        <source>Secondary Color</source>
        <translation>Secondary Colour</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSliders.qml" line="23"/>
        <source>Highlight Color</source>
        <translation>Highlight Colour</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSliders.qml" line="28"/>
        <source>Secondary Highlight Color</source>
        <translation>Secondary Highlight Colour</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSliders.qml" line="33"/>
        <source>Highlight Background Color</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ColorTextInput</name>
    <message>
        <location filename="../qml/components/ColorTextInput.qml" line="31"/>
        <source>Color input</source>
        <translation>Colour input</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorTextInput.qml" line="73"/>
        <source>Input value, tap to reset</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ColorTextInputs</name>
    <message>
        <location filename="../qml/components/ColorTextInputs.qml" line="13"/>
        <source>Primary Color</source>
        <translation>Primary Colour</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorTextInputs.qml" line="18"/>
        <source>Secondary Color</source>
        <translation>Secondary Colour</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorTextInputs.qml" line="23"/>
        <source>Highlight Color</source>
        <translation>Highlight Colour</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorTextInputs.qml" line="28"/>
        <source>Secondary Highlight Color</source>
        <translation>Secondary Highlight Colour</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorTextInputs.qml" line="33"/>
        <source>Highlight Background Color</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CoverPage</name>
    <message>
        <location filename="../qml/cover/CoverPage.qml" line="22"/>
        <source>ThemeColor</source>
        <translation>ThemeColour</translation>
    </message>
</context>
<context>
    <name>FirstPage</name>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="27"/>
        <source>Adjust Theme Colors</source>
        <translation>Adjust Theme Colours</translation>
    </message>
    <message>
        <source>Current Color Model:</source>
        <translation type="vanished">Current Colour Model:</translation>
    </message>
    <message>
        <source>Primary Color</source>
        <translation type="vanished">Primary Colour</translation>
    </message>
    <message>
        <source>Secondary Color</source>
        <translation type="vanished">Secondary Colour</translation>
    </message>
    <message>
        <source>Highlight Color</source>
        <translation type="vanished">Highlight Colour</translation>
    </message>
    <message>
        <source>Secondary Highlight Color</source>
        <translation type="vanished">Secondary Highlight Colour</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="29"/>
        <source>Showroom</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="46"/>
        <source>Laboratory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="59"/>
        <source>Input Mode:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="60"/>
        <source>Tap to switch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>gray</source>
        <translation type="vanished">grey</translation>
    </message>
    <message>
        <source>Apply colors to system</source>
        <translation type="vanished">Apply colours to system</translation>
    </message>
    <message>
        <source>Reload colors from system</source>
        <translation type="vanished">Reload colors from system</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="104"/>
        <source>Apply Colors to System</source>
        <translation>Apply Colours to System</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="107"/>
        <source>Reload Colors from System</source>
        <translation>Reload Colours from System</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="112"/>
        <source>Experimental or dangerous actions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="113"/>
        <source>Export to Ambience file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="113"/>
        <location filename="../qml/pages/FirstPage.qml" line="117"/>
        <source>(not implemented)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="117"/>
        <source>Save Theme to current Ambience</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="118"/>
        <source>Saving...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="121"/>
        <source>Remove custom values from configuration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="122"/>
        <source>Resetting...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="124"/>
        <source>Restart Lipstick</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="125"/>
        <source>Restarting...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="98"/>
        <source>Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="101"/>
        <source>Open Ambience Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Apply Colors to current Theme</source>
        <translation type="vanished">Apply colours to current Theme</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="105"/>
        <source>Applying...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Reload Colors</source>
        <translation type="vanished">Reload Colours</translation>
    </message>
    <message>
        <source>Color</source>
        <translation type="vanished">Colour</translation>
    </message>
    <message>
        <source>Reset Colors</source>
        <translation type="vanished">Reset Colours</translation>
    </message>
</context>
<context>
    <name>HelpPage</name>
    <message>
        <source>Use the Sliders in the lower section to adjust the colors.&lt;br /&gt;            When satisfied, tap the area above the slider to set the color.            Check what your theme will look like in the Showroom display.&lt;br /&gt;            &lt;br /&gt;            When you&apos;re done, use the PullUp Manu or the PushUp Menu to apply your creation.</source>
        <translation type="vanished">Use the Sliders in the lower section to adjust the colours.&lt;br /&gt;            When satisfied, tap the area above the slider to set the colour.            Check what your theme will look like in the Showroom display.&lt;br /&gt;            &lt;br /&gt;            When you&apos;re done, use the PullUp Manu or the PushUp Menu to apply your creation.</translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="19"/>
        <source>How to Use</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="26"/>
        <source>General</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>This application allows you to modify the current color scheme of Lipstick. It will not (yet) however, change or create new ambiences,           nor will your changes survive an Ambience change, Lipstick restart, or device reboot.&lt;br /&gt;
          Currently, only four colors can be edited: primaryColor, secondaryColor, highlightColor, and secondaryHighlightColor, as these are the ones saved by the system.          There are other colors in use by the system, which are autocomputed from these basic four, and can not be modified at the moment.&lt;br/&gt;&lt;br/&gt;
          We are working on overcoming some of these limitations.</source>
        <translation type="vanished">This application allows you to modify the current colour scheme of Lipstick. It will not (yet) however, change or create new ambiences,           nor will your changes survive an Ambience change, Lipstick restart, or device reboot.&lt;br /&gt;
          Currently, only four colours can be edited: primaryColor, secondaryColor, highlightColor, and secondaryHighlightColor (sic!), as these are the ones saved by the system.          There are other colours in use by the system, which are autocomputed from these basic four, and can not be modified at the moment.&lt;br/&gt;&lt;br/&gt;
          We are working on overcoming some of these limitations.</translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="28"/>
        <source>This application allows you to modify the current color scheme of Lipstick. It will not (yet) however, change or create new ambiences,           nor will your changes survive an Ambience change, Lipstick restart, or device reboot.&lt;br /&gt;
          Currently, only some colors can be edited.          There are other colors in use by the system, which are autocomputed from the basic four, and can not be modified at the moment.&lt;br/&gt;&lt;br/&gt;
          We are working on overcoming some of these limitations.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="34"/>
        <source>The Showroom</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="36"/>
        <source>The Top area on the first page (&quot;Showroom&quot;) is non-interactive          and just shows the colors that are selected currently.&lt;br /&gt;          Here you can preview your creation.          </source>
        <translation>The Top area on the first page (&quot;Showroom&quot;) is non-interactive          and just shows the colours that are selected currently.&lt;br /&gt;          Here you can preview your creation.          </translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="41"/>
        <source>The Laboratory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="43"/>
        <source>In Slider Input Mode, use the Sliders in the lower section to adjust the colors.&lt;br /&gt;            In Text Input Mode, you can enter color values directly.&lt;br /&gt;
            Check what your theme will look like in the Showroom display.&lt;br /&gt;            &lt;br /&gt;            When you&apos;re done, use the PullUp Menu or the PushUp Menu to apply your creation to the Session.
          </source>
        <translation>In Slider Input Mode, use the Sliders in the lower section to adjust the colours.&lt;br /&gt;            In Text Input Mode, you can enter colour values directly.&lt;br /&gt;
            Check what your theme will look like in the Showroom display.&lt;br /&gt;            &lt;br /&gt;            When you&apos;re done, use the PullUp Menu or the PushUp Menu to apply your creation to the Session.
          </translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="50"/>
        <source>The Cupboard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="52"/>
        <source>This area allows you to store your created palettes for re-use later.             There is one global Cupboard, and one specific for the current ambience.&lt;br /&gt;             Note that only system-wide Ambiences have a name, custon ones will show as anonymous (for now)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LootBoxItem</name>
    <message>
        <location filename="../qml/components/LootBoxItem.qml" line="21"/>
        <location filename="../qml/components/LootBoxItem.qml" line="26"/>
        <source>ThemeColor</source>
        <translation>ThemeColour</translation>
    </message>
    <message>
        <location filename="../qml/components/LootBoxItem.qml" line="25"/>
        <source>A Lootbox was delivered!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/LootBoxItem.qml" line="26"/>
        <source>now has more shelves!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/LootBoxItem.qml" line="26"/>
        <source>Your persistence has been rewarded.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/LootBoxItem.qml" line="27"/>
        <source>Your persistence has been rewarded!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/LootBoxItem.qml" line="37"/>
        <source>Purchase Options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/LootBoxItem.qml" line="46"/>
        <source>Payment* received!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/LootBoxItem.qml" line="46"/>
        <source>Buy more shelves</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/LootBoxItem.qml" line="51"/>
        <source>Using Jolla Shop credentials to buy Storage Lootbox</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/LootBoxItem.qml" line="64"/>
        <source>Thank you for your purchase!&lt;br /&gt;Your extra shelves will be delivered in the next update!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/LootBoxItem.qml" line="74"/>
        <source>*) No seriously there are no in-app-purchases in this app or Lootboxes in the Jolla Shop. That would be ridiculous.&lt;br /&gt;No funds were transfered.&lt;br /&gt; In fact, nothing really happened just now.&lt;br /&gt;...or did it?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SaveSlot</name>
    <message>
        <location filename="../qml/components/SaveSlot.qml" line="37"/>
        <source>Ambience</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/SaveSlot.qml" line="37"/>
        <source>Shelf</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/SaveSlot.qml" line="60"/>
        <source>Take to Lab</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/SaveSlot.qml" line="61"/>
        <source>Put on Shelf</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Saver</name>
    <message>
        <location filename="../qml/pages/Saver.qml" line="63"/>
        <source>Global Cupboard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Saver.qml" line="96"/>
        <source>Clean out this cupboard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Saver.qml" line="96"/>
        <source>Spring Clean...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SaverPlus</name>
    <message>
        <location filename="../qml/pages/SaverPlus.qml" line="11"/>
        <source>anonymous</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SaverPlus.qml" line="94"/>
        <source>Ambience Cupboard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SaverPlus.qml" line="138"/>
        <source>Clean out this cupboard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SaverPlus.qml" line="138"/>
        <source>Spring Clean...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ShowRoom</name>
    <message>
        <location filename="../qml/components/ShowRoom.qml" line="88"/>
        <location filename="../qml/components/ShowRoom.qml" line="89"/>
        <location filename="../qml/components/ShowRoom.qml" line="90"/>
        <location filename="../qml/components/ShowRoom.qml" line="91"/>
        <location filename="../qml/components/ShowRoom.qml" line="92"/>
        <source>A very long line showing Text in </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Color</source>
        <translation type="vanished">Colour</translation>
    </message>
    <message>
        <location filename="../qml/components/ShowRoom.qml" line="88"/>
        <location filename="../qml/components/ShowRoom.qml" line="107"/>
        <location filename="../qml/components/ShowRoom.qml" line="135"/>
        <source>Primary Color</source>
        <translation>Primary Colour</translation>
    </message>
    <message>
        <location filename="../qml/components/ShowRoom.qml" line="89"/>
        <location filename="../qml/components/ShowRoom.qml" line="113"/>
        <location filename="../qml/components/ShowRoom.qml" line="141"/>
        <source>Secondary Color</source>
        <translation>Secondary Colour</translation>
    </message>
    <message>
        <location filename="../qml/components/ShowRoom.qml" line="90"/>
        <source>Highlight Color</source>
        <translation>Highlight Colour</translation>
    </message>
    <message>
        <location filename="../qml/components/ShowRoom.qml" line="91"/>
        <source>Secondary Highlight Color</source>
        <translation>Secondary Highlight Colour</translation>
    </message>
    <message>
        <location filename="../qml/components/ShowRoom.qml" line="92"/>
        <source>Error Color</source>
        <translation>Error Colour</translation>
    </message>
    <message>
        <location filename="../qml/components/ShowRoom.qml" line="107"/>
        <location filename="../qml/components/ShowRoom.qml" line="113"/>
        <location filename="../qml/components/ShowRoom.qml" line="135"/>
        <location filename="../qml/components/ShowRoom.qml" line="141"/>
        <source>Background Color</source>
        <translation>Background Colour</translation>
    </message>
    <message>
        <source>four kinds of background overlay opacities and colors</source>
        <translation type="vanished">four kinds of background overlay opacities and colours</translation>
    </message>
    <message>
        <location filename="../qml/components/ShowRoom.qml" line="107"/>
        <location filename="../qml/components/ShowRoom.qml" line="113"/>
        <location filename="../qml/components/ShowRoom.qml" line="135"/>
        <location filename="../qml/components/ShowRoom.qml" line="141"/>
        <source>Text</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/ShowRoom.qml" line="251"/>
        <location filename="../qml/components/ShowRoom.qml" line="252"/>
        <source>Button</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
